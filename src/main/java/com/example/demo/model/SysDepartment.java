package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;


/**
 * @author YZD
 */
@Entity
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SysDepartment implements java.io.Serializable {

    private static final long serialVersionUID = 6168625386984020039L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "部门名称不为空！")
    private String name;


    private Long parentId;


    private Integer level;


    private String description;


    private String contactPerson;


    private String contactPhone;


    private String address;

    private String code;

    private String departmentCode;

    @ApiModelProperty(value = "部门使用量")
    @Transient
    private Integer value;

    @ApiModelProperty(value = "子部门")
    @Transient
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<SysDepartment> children;

}
