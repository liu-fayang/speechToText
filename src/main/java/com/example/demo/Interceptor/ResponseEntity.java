package com.example.demo.Interceptor;

import lombok.Data;

import java.io.Serializable;

/**
 * 统一格式类
 *
 * @author : YZD
 * @date : 2021-7-28 16:19
 */
@Data
public class ResponseEntity<T> implements Serializable {

    private static final long serialVersionUID = 8960474786737581150L;

    /**
     * 错误码
     */
    private Integer status;
    /**
     * 提示信息
     */
    private String msg;
    /**
     * 具体内容
     */
    private T data;

    public static <T> ResponseEntity<T> builderResponse(int status, String msg, T data) {
        ResponseEntity<T> res = new ResponseEntity<T>();
        res.setStatus(status);
        res.setMsg(msg);
        res.setData(data);
        return res;
    }

    public static <T> ResponseEntity<T> success(String msg) {
        return builderResponse(200, msg, null);
    }
    public static <T> ResponseEntity<T> success(String msg, T data) {
        return builderResponse(200, msg, data);
    }
    public static <T> ResponseEntity<T> success(T data) {
        return builderResponse(200, "成功!", data);
    }
    public static <T> ResponseEntity<T> success() {
        return builderResponse(200, "成功!", null);
    }

    public static <T> ResponseEntity<T> error() {
        return builderResponse(500, "服务器错误!", null);
    }
    public static <T> ResponseEntity<T> error(String msg) {
        return builderResponse(500, msg, null);
    }

    public static <T> ResponseEntity<T> error(int status, String msg) {
        return builderResponse(status, msg,   (T) null);
    }

    public static <T> ResponseEntity<T> error(T date) {
        return builderResponse(500, "服务器错误!", date);
    }

    public static <T> ResponseEntity<T> illegalRequest() {
        return builderResponse(403, "请求非法!", (T) null);
    }

    public static <T> ResponseEntity<T> exception() {
        return builderResponse(501, "服务器错误!", (T) null);
    }

    public static <T> ResponseEntity<T> paramsEmpty() {
        return builderResponse(400, "请求参数错误!", (T) null);
    }
}
